import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/components/base/Login.vue'
import Register from '@/components/base/Register.vue'
import PrincipalPage from '@/views/PrincipalPage.vue'
import Error404Page from '@/views/Error404Page.vue'
import Contribuyentes from '@/components/vistas/Contribuyente.vue'
import Reciclador from '@/components/vistas/Reciclador.vue'
import ResiduosPendientes from '@/components/vistas/Residuos_Pendientes.vue'
import ResiduosAsigandos from '@/components/vistas/Residuos_Asignadas.vue'
import ResiduosRecogidos from '@/components/vistas/Residuos_Recogidos.vue'
import Usuarios from '@/components/vistas/Usuarios.vue'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'login',
        component: Login
    },
    {
        path: '/register',
        name: 'register',
        component: Register
    },
    {
        path: '/home',
        // name: 'home',
        component: PrincipalPage,
        children: [{
                path: '',
                component: Reciclador,
                name: 'home'
            },
            {
                path: 'contribuyentes',
                component: Contribuyentes,
                name: 'contribuyentes'
            },
            {
                path: 'residuos_pendientes',
                component: ResiduosPendientes,
                name: 'residuos_pendientes'
            },

            {
                path: 'residuos_asignados',
                component: ResiduosAsigandos,
                name: 'residuos_asignados'
            },
            {
                path: 'residuos_recogidos',
                component: ResiduosRecogidos,
                name: 'residuos_recogidos'
            },
            
            {
                path: 'usuarios',
                component: Usuarios,
                name: 'usuarios'
            }
        ]
    },
    {
        path: '/:pathMatch(.*)*',
        component: Error404Page,
    },


]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router