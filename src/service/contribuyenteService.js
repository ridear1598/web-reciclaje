import Service from './service.js'

// const resourse = 'api/users/getAllContribuyentesAndRecicladores';

export default {
    get() {
        let lista = Service.get(`api/users/getAllContribuyentesAndRecicladores/1`);
        return lista;
    },

    create(data) {
        return Service.post(`api/users/registerContribuyenteOrReciclador/1`, data)
    },

    update(data){
        return Service.put(`/api/users/updateWeb`,data.contribuyente);
    },
    delete(data) {
        return Service.delete(`api/contribuyente/${data.id}`)
    }

}