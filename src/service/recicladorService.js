import Service from './service.js'

// const resourse = 'api/reciclador';

export default {
    get() {
        let lista = Service.get(`api/users/getAllContribuyentesAndRecicladores/3`);
        return lista;
    },
    create(data) {
        return Service.post(`api/users/registerContribuyenteOrReciclador/3`, data)
    },
    update(data){
        return Service.put(`/api/users/updateWeb`,data.reciclador);
    },
    delete(data) {
        return Service.delete(`api/reciclador/${data.id}`)
    }

}