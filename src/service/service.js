import axios from 'axios';


// const baseUrl = 'http://192.168.1.3:5000/';

const baseUrl = 'https://back-reciclaje.herokuapp.com'
export default axios.create({
    baseURL: baseUrl
});
