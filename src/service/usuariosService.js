import Service from './service.js'

const resourse = 'usuario';

export default {
    get() {
        let lista = Service.get(resourse);
        return lista;
    },
    login(data){
        console.log(data)
      return Service.post(`usuario/login`, data)
    },
    create(data) {
        return Service.post(resourse, data)
    },
    update(data){
        return Service.put(`reciclador/${data.id}`,data.reciclador)
    },
    delete(data) {
        return Service.delete(`reciclador/${data.id}`)
    }

}