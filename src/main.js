import Vue from 'vue'
import App from './App.vue'
import router from './router'

import 'bootstrap'
import "@/assets/js/all.min.js"
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import "./assets/css/styles.css";
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(VueSweetalert2); 

Vue.config.productionTip = false


new Vue({
    router,
    render: h => h(App)
}).$mount('#app')